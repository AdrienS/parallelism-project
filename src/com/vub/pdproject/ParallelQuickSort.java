package com.vub.pdproject;

import com.vub.pdproject.search.QueryEngine;

import java.util.ArrayList;

/**
 * Created by adrienschautteet on 23/03/17.
 */
public class ParallelQuickSort<T extends Comparable<T>> {

    private ArrayList<T> list;
    private int left;
    private int right;

    public ParallelQuickSort (ArrayList<T> list, int left, int right) {
        this.list = list;
        this.left = left;
        this.right = right;
    }

    static public <T extends Comparable<T>> ArrayList<T> Sort(ArrayList<T> list) {

        ArrayList<T> part = new ArrayList<>();

        if (list.size() != 0) {

            int pivot = (int) Math.floor((0 + list.size()-1) / 2);

            T pivotValue = list.get(pivot);

            ArrayList<T> leftPart = Pack.packParallel(list, i -> i.compareTo(pivotValue) <= 0  ? true : false);
            ArrayList<T> rightPart = Pack.packParallel(list, i -> i.compareTo(pivotValue) > 0 ? true : false);

            // Delete pivot element from left or right partitions
            leftPart.remove(pivotValue);
            rightPart.remove(pivotValue);

            part.addAll(ParallelQuickSort.Sort(leftPart));
            part.add(pivotValue);
            part.addAll(ParallelQuickSort.Sort(rightPart));

        }
        return part;
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(21);
        list.add(4);
        list.add(11);
        list.add(9);
        list.add(7);
        list.add(3);
        list.add(15);
        list.add(2);
        list.add(7);


        ArrayList<String> slist = new ArrayList<>();
        slist.add("Hello");
        slist.add("From");
        slist.add("Planet");
        slist.add("Earth");
        slist.add("Earth");

        boolean test = slist.get(1).compareTo("From") < 0;


        list = ParallelQuickSort.Sort(list);//, 0, list.size()-1);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + "\n");
        }
    }
}
