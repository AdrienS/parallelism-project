package com.vub.pdproject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

/**
 * Created by adrienschautteet on 25/03/17.
 */
public class Pack<T> extends RecursiveAction {

    ArrayList<T> input;
    ArrayList<T> output;
    int lo;
    int hi;
    ArrayList<Integer> bitVector;
    ArrayList<Integer> bitSum;

    int SEQUENTIAL_THRESHOLD = 2;

    @FunctionalInterface
    public interface Filter<T> {
        boolean filter(T element);
    }

    public Pack(ArrayList<T> input, ArrayList<T> output, int lo, int hi, ArrayList<Integer> bitVector, ArrayList<Integer> bitSum) {
        this.input = input;
        this.output = output;
        this.lo = lo;
        this.hi = hi;
        this.bitVector = bitVector;
        this.bitSum = bitSum;
    }
    protected void compute() {
        if ((hi - lo) < SEQUENTIAL_THRESHOLD) {
            for (int i = lo; i < hi; i++) {
                if (bitVector.get(i) == 1) {
                    output.set((bitSum.get(i) - 1), input.get(i));
                }
            }
        } else {
            int mid = (lo + hi) / 2;
            Pack left = new Pack(input, output, lo, mid, bitVector, bitSum);
            Pack right = new Pack(input, output, mid, hi, bitVector, bitSum);
            left.fork();
            right.compute();
            left.join();
        }
    }
    final static ForkJoinPool fjPool = ForkJoinPool.commonPool();

    static <T> ArrayList<T> packParallel(ArrayList<T> input, Filter<T> filter){
        // Phase 1
        final ArrayList<Integer> bitVector = input.stream().map(i -> filter.filter(i) ?  1 : 0).collect(Collectors.toCollection(ArrayList::new));
        // Phase 2
        final ArrayList<Integer> bitSum = PrefixSum.parallelPrefixSum(bitVector);
        // Phase 3
        int outputCap = bitSum.get(bitSum.size()-1);
        final ArrayList<T> output = new ArrayList<>(Collections.nCopies(outputCap, null));
        fjPool.invoke(new Pack(input,output,0,input.size(),bitVector,bitSum));
        return output;
    }

    public static void main(String[] args) {
        ArrayList<Integer> intArray = new ArrayList<>(Arrays.asList(5, 2, -1, 6, -34, 9, -43, 0));

        intArray = packParallel(intArray, i -> i >= 5);
        for (int i : intArray) {
            System.out.println(i);
        }
    }
}
