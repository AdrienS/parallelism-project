package com.vub.pdproject;

import com.vub.pdproject.search.ParallelSearch;
import com.vub.pdproject.search.QueryEngine;
import com.vub.pdproject.search.SequentialSearch;
import com.vub.pdproject.search.YelpQuery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by adrienschautteet on 26/04/17.
 */
public class CalculatePerformance {

    enum Mode {
        SEQUENTIAL,
        PARALLEL
    }

    public static void main(String[] args) {
        int n = 1000;
        Measure2a(n);
        Measure2b(n);
    }

    public static List<Long> CalculateRuntimes(Mode mode, int p, int T, int n, int bench) {

        List<Long> runtimes = new ArrayList<>();
        long startTime, endTime, duration;
        YelpQuery query = YelpQuery.forBenchmark(bench);
        String info = mode + "_" + p + "p_" + T + "T_" + query.getKeyword();
        File csv_file = new File("Measurements.csv");

        switch (mode) {
            case SEQUENTIAL:
                QueryEngine sequential = new SequentialSearch();
                // Calculate sequential
                for (int i = 0; i < n; i++) {
                    startTime = System.nanoTime();
                    query.execute(sequential);
                    endTime = System.nanoTime();
                    duration = (endTime - startTime);
                    runtimes.add(duration);
                }
                break;
            case PARALLEL:
                QueryEngine parallel = new ParallelSearch(p, T);
                // Calcualte Parallel
                for (int i = 0; i < n; i++) {

                    // Warmup forkjoin library
                    for (int j = 0; j < 5 && i == 0; j++) {
                        query.execute(parallel);
                    }
                    startTime = System.nanoTime();
                    //-----------

                    query.execute(parallel);

                    //-----------
                    endTime = System.nanoTime();
                    duration = (endTime - startTime);

                    runtimes.add(duration);
                }
                break;
            default:
                break;
        }
        write2file(csv_file, runtimes, info);
        return runtimes;
    }

    public static void Measure2a(int n) {
        // Index/value => 0/sequential, 1/T1, 2/T2, 3/T4, 4/T8
        ArrayList<List<Long>> meanRuntimes2a = new ArrayList<>();

        // eCSUTp = error on computational speedup mean for Tp
        double overhead, compSpeedUpT1, compSpeedUpT2, compSpeedupT4, compSpeedUpT8, eCSUT1, eCSUT2, eCSUT4, eCSUT8;
        final int INF = Integer.MAX_VALUE;

        System.out.println("######################################## Part 2a ########################################");
        for (int i = 1; i <= 3; i++) {

            meanRuntimes2a.add(0, CalculateRuntimes(Mode.SEQUENTIAL, 0, 0, n, i));
            meanRuntimes2a.add(1, CalculateRuntimes(Mode.PARALLEL, 1, INF, n, i));
            meanRuntimes2a.add(2, CalculateRuntimes(Mode.PARALLEL, 2, INF, n, i));
            meanRuntimes2a.add(3, CalculateRuntimes(Mode.PARALLEL, 4, INF, n, i));
            meanRuntimes2a.add(4, CalculateRuntimes(Mode.PARALLEL, 8, INF, n, i));

            overhead = Estimate.meanRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(0));

            eCSUT1 = Estimate.errorRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(1));
            eCSUT2 = Estimate.errorRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(2));
            eCSUT4 = Estimate.errorRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(3));
            eCSUT8= Estimate.errorRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(4));

            compSpeedUpT1 = Estimate.meanRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(1));
            compSpeedUpT2 = Estimate.meanRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(2));
            compSpeedupT4 = Estimate.meanRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(3));
            compSpeedUpT8 = Estimate.meanRuntimeRatio(meanRuntimes2a.get(1), meanRuntimes2a.get(4));

            System.out.println("Results for benchmark," + i);
            System.out.println("Overhead," + overhead);
            System.out.println("Computational speedup for T1," + compSpeedUpT1);
            System.out.println("Computational speedup for T2," + compSpeedUpT2);
            System.out.println("Computational speedup for T4," + compSpeedupT4);
            System.out.println("Computational speedup for T8," + compSpeedUpT8);
            System.out.println("SE for T1," + eCSUT1);
            System.out.println("SE for T2," + eCSUT2);
            System.out.println("SE for T4," + eCSUT4);
            System.out.println("SE for T8," + eCSUT8);
        }
    }

    public static void Measure2b(int n) {

        int[] thresholds = {
                1, 50, 500, 1000, 1500, 2000, 2500, 5000, 7500, 10000, 50000, 100000, 500000, Integer.MAX_VALUE};
        double overhead, appSpeedUp, overheadSE, appSpeedUpSE;

        List<Long> runtimesSequential;
        List<Long> runtimesT4;

        System.out.println("######################################## Part 2b ########################################");
        System.out.println("overhead,\t overhead SE,\t application speedup,\t application speedup SE");
        for (int b = 1; b <= 3; b++) {
            runtimesSequential = CalculateRuntimes(Mode.SEQUENTIAL, 0, 0, n, b);

            for (int T : thresholds) {
                runtimesT4 = CalculateRuntimes(Mode.PARALLEL, 4, T, n, b);
                overhead = Estimate.meanRuntimeRatio(runtimesT4, runtimesSequential);
                overheadSE = Estimate.errorRuntimeRatio(runtimesT4, runtimesSequential);
                appSpeedUp = Estimate.meanRuntimeRatio(runtimesSequential, runtimesT4);
                appSpeedUpSE = Estimate.errorRuntimeRatio(runtimesSequential, runtimesT4);

                System.out.println(overhead + ",\t" + overheadSE + ",\t" + appSpeedUp + ",\t" + appSpeedUpSE);
            }
        }

    }

    // Based on Steven Adriaensen's implementation of Session 3 - Benchmarking
    static private void write2file(File file, List<Long> runtimes, String info){
        PrintWriter csv_writer;
        //File file = new File("runtimes_" + info + ".csv");
        try {
            csv_writer = new PrintWriter(new FileOutputStream(file,true));
            String line = info ;
            for(Long rt : runtimes){
                line += ","+rt;
            }
            csv_writer.println(line);
            csv_writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
