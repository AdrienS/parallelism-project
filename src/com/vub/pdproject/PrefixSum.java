package com.vub.pdproject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Getting in touch with parallel prefix-sum/prefix-scan algorithms.
 * 
 * @author Stefan Marr
 */
public class PrefixSum {

    /**
     * First phase of the parallel prefix sum builds up a tree of intermediate
     * results
     * 
     * @author Stefan Marr
     */
    public static class BuildNodeTask extends RecursiveTask<TempTreeNode> {
        private static final long serialVersionUID = -742809860551941054L;
        private final ArrayList<Integer> input;
        private final int         lo;
        private final int         hi;

        public BuildNodeTask(ArrayList<Integer> input, int lo, int hi) {
            this.input = input;
            this.lo = lo;
            this.hi = hi;
        }

        @Override
        protected TempTreeNode compute() {
            if (hi - lo < 2) {
                return new TempTreeNode(null, null, input.get(lo), lo, hi);
            } else {
                BuildNodeTask left  = new BuildNodeTask(input, lo,            (hi + lo) / 2);
                BuildNodeTask right = new BuildNodeTask(input, (hi + lo) / 2,            hi);

                left.fork();

                TempTreeNode rightTreeNode = right.compute();
                TempTreeNode leftTreeNode  = left.join();
                return new TempTreeNode(leftTreeNode, rightTreeNode,
                        leftTreeNode.sum + rightTreeNode.sum, lo, hi);
            }
        }
    }

    public static class SumTask extends RecursiveAction {
        private static final long  serialVersionUID = -2039136112928941061L;
        private final TempTreeNode treeNode;
        private final int          leftSum;
        private final ArrayList<Integer> input;
        private final ArrayList<Integer> result;

        public SumTask(TempTreeNode treeNode, int leftSum, ArrayList<Integer> input,
                       ArrayList<Integer> result) {
            this.treeNode = treeNode;
            this.leftSum = leftSum;
            this.input = input;
            this.result = result;
        }

        @Override
        protected void compute() {
            if (treeNode.isLeaf()) {
                result.set(treeNode.lo, input.get(treeNode.lo) + leftSum);
            } else {
                SumTask left = new SumTask(treeNode.leftNode, leftSum, input,
                        result);
                SumTask right = new SumTask(treeNode.rightNode, leftSum
                        + treeNode.leftNode.sum, input, result);

                left.fork();
                right.compute();
                left.join();
            }
        }
    }

    /**
     * The {} task will build a binary tree consisting of these
     * Nodes. Leaf nodes have their left and right field set to <tt>null</tt>.
     * 
     * Each node stores the sum of all elements in range [lo,hi[.
     */
    public static class TempTreeNode {
        public final TempTreeNode leftNode;
        public final TempTreeNode rightNode;

        public final int          sum;

        public final int          lo;
        public final int          hi;

        TempTreeNode(TempTreeNode left, TempTreeNode right, int sum, int lo,
                int hi) {
            leftNode = left;
            rightNode = right;

            this.sum = sum;

            this.lo = lo;
            this.hi = hi;
        }

        public boolean isLeaf() {
            return leftNode == null && rightNode == null;
        }
    }

    public static ArrayList<Integer> parallelPrefixSum(ArrayList<Integer> input) {
        ArrayList<Integer> result = new ArrayList(Collections.nCopies(input.size(), 0));

        TempTreeNode rootNode = forkJoinPool.invoke(new BuildNodeTask(input, 0,
                input.size()));

        forkJoinPool.invoke(new SumTask(rootNode, 0, input, result));

        return result;
    }

    static final ForkJoinPool forkJoinPool = new ForkJoinPool(4);
}
