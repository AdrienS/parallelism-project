package com.vub.pdproject.search;

import com.vub.pdproject.Util;
import com.vub.pdproject.data.YelpData;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by adrienschautteet on 26/03/17.
 */
public class AccumulateOccurencesTask extends RecursiveTask<Integer> {

    static final long serialVersionUID = 1L;

    YelpData data;
    static String kw;
    List<String> reviews;
    int lo, hi, T;

    AccumulateOccurencesTask(YelpData data, List<String> reviews, String kw, int T, int lo, int hi) {
        this.data = data;
        this.kw = kw;
        this.reviews = reviews;
        this.T = T;
        this.lo = lo;
        this.hi = hi;
    }

    AccumulateOccurencesTask(YelpData data, List<String> reviews, String kw, int T) {
        this(data, reviews, kw, T, 0, reviews.size());
    }

    @Override
    protected Integer compute() {
        if (hi - lo <= 1) {
            int occurences = 0;
            for (int i = lo; i < hi; i++) {
                String review = data.getReview(reviews.get(i)).text;
                occurences += new CountOccurencesTask(review).compute();
            }
            return occurences;
        } else {
            AccumulateOccurencesTask left = new AccumulateOccurencesTask(data, reviews, kw, T, lo, (hi + lo) / 2);
            AccumulateOccurencesTask right = new AccumulateOccurencesTask(data, reviews, kw, T, (hi + lo) / 2, hi);
            left.fork();
            int rightAns = right.compute();
            int leftAns = left.join();
            return leftAns + rightAns;
        }
    }

    protected class CountOccurencesTask extends RecursiveTask<Integer> {

        String review;
        int lo, hi, leftAns, rightAns;

        CountOccurencesTask(String review) {
            this(review, 0, review.length());
        }

        CountOccurencesTask(String review, int lo, int hi) {
            this.review = review;
            this.lo = lo;
            this.hi = hi;
        }

        @Override
        protected Integer compute() {
            if (hi-lo <= T) {
                int count  = ParallelSearch.countOccurrences(kw, review, lo, hi);
                return count;
            } else {
                int middle = (hi + lo) / 2;
                // While middle is not at space or pm and smaller than the length of the current substring
                while (middle < review.length()) {
                    if (Util.isWhitespaceOrPunctuationMark(review.charAt(middle))) {
                        break;
                    }
                    middle++;
                }
                // If we have a singe word, go sequential. Otherwise paralellize.
                if (middle == hi) {
                    int count  = ParallelSearch.countOccurrences(kw, review, lo, hi);
                    return count;
                } else {
                    CountOccurencesTask left = new CountOccurencesTask(review, lo, middle);
                    CountOccurencesTask right = new CountOccurencesTask(review, middle, hi);
                    left.fork();
                    rightAns = right.compute();
                    leftAns = left.join();
                }
            }
            return leftAns + rightAns;
        }
    }
}