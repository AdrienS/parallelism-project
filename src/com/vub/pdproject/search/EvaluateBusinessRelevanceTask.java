package com.vub.pdproject.search;

import com.vub.pdproject.data.YelpData;
import com.vub.pdproject.data.models.Business;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by adrienschautteet on 25/03/17.
 */
public class EvaluateBusinessRelevanceTask extends RecursiveTask<List<QueryEngine.RRecord>> {

    private static final long serialVersionUID = 1L;

    YelpData data;
    String kw;
    int T;
    int lo;
    int hi;

    EvaluateBusinessRelevanceTask(YelpData data, String kw, int T, int lo, int hi) {
        this.data = data;
        this.kw = kw;
        this.T = T;
        this.lo = lo;
        this.hi = hi;
    }

    public EvaluateBusinessRelevanceTask(YelpData data, String kw, int T) {
        this(data, kw, T, 0, data.getBusinessIDs().size());
    }

    @Override
    protected List<QueryEngine.RRecord> compute() {
        // Threshold placeholder here for testing
        if (hi - lo <= 1) {
            List<QueryEngine.RRecord> res = new ArrayList<>();
            for (int i = lo; i < hi; i++) {
                // Get a business by index
                Business b = data.getBusiness(data.getBusinessIDs().get(i));
                // Calculate occurences of a given keyword in all the reviews of a given business
                int occurences = new AccumulateOccurencesTask(data, b.reviews, kw, T).compute();

                double relevance = CalculateRelevanceScore(b, kw, occurences);
                if (relevance > 0) {
                    res.add(new QueryEngine.RRecord(data.getBusinessIDs().get(i), relevance));
                }
            }
            return res;
        } else {
            EvaluateBusinessRelevanceTask left = new EvaluateBusinessRelevanceTask(data, kw, T, lo, (hi + lo) / 2);
            EvaluateBusinessRelevanceTask right = new EvaluateBusinessRelevanceTask(data, kw, T, (hi + lo) / 2, hi);
            left.fork();

            List<QueryEngine.RRecord> rightAns = right.compute();
            List<QueryEngine.RRecord> leftAns = left.join();

            return RRecordsMergeSort(leftAns, rightAns);
        }
    }

    private double CalculateRelevanceScore(Business b, String kw, int occurences) {
        double relevance_score = 0;
        if (SequentialSearch.countOccurrences(kw, b.name) > 0) {
            relevance_score = 0.5;
        }
        relevance_score += 1.5 * occurences / (occurences + 20);
        relevance_score *= b.stars;
        return relevance_score;
    }

    private List<QueryEngine.RRecord> RRecordsMergeSort(List<QueryEngine.RRecord> left, List<QueryEngine.RRecord> right) {
        int leftIdx = 0;
        int rightIdx = 0;

        List<QueryEngine.RRecord> result = new ArrayList<>();

        while (leftIdx < left.size() && rightIdx < right.size()) {
            if (left.get(leftIdx).relevance_score > right.get(rightIdx).relevance_score) {
                result.add(left.get(leftIdx));
                leftIdx++;
            } else {
                result.add(right.get(rightIdx));
                rightIdx++;
            }
        }

        result.addAll(left.subList(leftIdx, left.size()));
        result.addAll(right.subList(rightIdx, right.size()));

        return result;
    }
}