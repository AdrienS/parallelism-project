package com.vub.pdproject.search;

import java.util.*;
import java.util.concurrent.ForkJoinPool;

import com.vub.pdproject.Util;
import com.vub.pdproject.data.YelpData;

/**
 * TODO: A parallel implementation of QueryEngine using Java Fork-join
 * (see assignment for detailed requirements of this implementation)
 * 
 * This is normally the only file you should change (except for Main.java for testing/evaluation).
 * If you for some reason feel the need to change another existing file, 
 * contact Steven Adriaensen first, and mention this modification explicitly in the report.
 * Note that adding new files for modularity purposes is always ok.
 * 
 * @author You
 *
 */
public class ParallelSearch implements QueryEngine {
	final int p; //parallelism level (i.e. max. # cores that can be used by Java Fork/Join)
	final int T; //sequential threshold (semantics depend on your cut-off implementation)
	final ForkJoinPool fjPool;

	/**
	 * Creates a parallel search engine with p worker threads.
	 * Counting occurrences is to be done sequentially (T ~ +inf)
	 *
	 * @param p parallelism level
	 */
	ParallelSearch(int p) {
		this(p, Integer.MAX_VALUE);
	}

	/**
	 * Creates a parallel search engine with p worker threads and sequential cut-off threshold T.
	 *
	 * @param p parallelism level
	 * @param T sequential threshold
	 */
	public ParallelSearch(int p, int T) {
		this.p = p;
		this.T = T;
		//Hint: Initialise the Java Fork/Join framework here as well.
		fjPool = new ForkJoinPool(p);
	}

	@Override
	public List<RRecord> search(String query_str, YelpData data) {
		//TODO: implement this method using Java Fork-Join
		List<RRecord> relevant_business = fjPool.invoke(new EvaluateBusinessRelevanceTask(data, query_str, T));
		return relevant_business;
	}

	public static int countOccurrences(String keyword, String text, int lo, int hi){
		int count = 0;
		int k = 0;
		for (int i=lo; i < hi; i++){
			if(Util.isWhitespaceOrPunctuationMark(text.charAt(i))){
				if(k == keyword.length()){
					count++;
				}
				k = 0;
			}else if(k >= 0){
				if(k < keyword.length() && text.charAt(i) == keyword.charAt(k)){
					k++;
				}else{
					k = -1;
				}
			}
		}
		if(k == keyword.length()){
			count++;
		}
		return count;
	}
}
